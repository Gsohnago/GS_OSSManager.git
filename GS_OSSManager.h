//
//  GS_OSSManager.h
//  OSS
//
//  Created by 巩小鹏 on 2019/6/25.
//  Copyright © 2019. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GS_OSSManager : NSObject

+(GS_OSSManager *)sharedManager;


/**
 上传头像图片到oss服务器上
 
 @param picName 自定义的图片文件名
 @param imgData 图片Data数据
 @param complete 返回图片地址
 @param failed  返回error信息
 */
- (void)headimg_uploadImgToOssImgPicName:(NSString *)picName
                                 imgData:(NSData *)imgData
                                complete:(void (^)(NSString * imgUrl))complete
                                  failed:(void (^)(NSError * error))failed;



/**
 上传退货图片到oss服务器上
 
 @param picName 自定义的图片文件名
 @param imgData 图片Data数据
 @param complete 返回图片地址
 @param failed  返回error信息
 */
- (void)aftersalePic_uploadImgToOssImgPicName:(NSString *)picName
                                      imgData:(NSData *)imgData
                                     complete:(void (^)(NSString * imgUrl))complete
                                       failed:(void (^)(NSError * error))failed;





/**
 上传图片到oss服务器上
 
 @param imgFileName 自定义的图片文件名，名字可以随意写，后缀名不能缺少，切记每次图片名都唯一
 @param imgData 图片Data数据
 */
- (void)uploadImgToOssImgFileName:(NSString *)imgFileName imgData:(NSData *)imgData;
/**
 获取oss上的图片url地址
 
 @param imgFileName imgFileName 图片文件名
 @return 返回图片的url地址
 */
- (NSString *)getImgAddress:(NSString *)imgFileName;

@end

NS_ASSUME_NONNULL_END
