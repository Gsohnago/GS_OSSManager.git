# GS_OSSManager

#### Description
针对阿里OSS上传头像的简单封装

###前言：
今天分享一下最近项目中用到的阿里OSS上传服务中其中一个小模块，我进行了一个简单的封装OSSMansger，OSS上传头像，上传成功后获取图片Url地址的封装，一句话解决你上传头像的难题。

[如何安装 OSS iOS SDK](https://help.aliyun.com/document_detail/32056.html?spm=a2c4g.11186623.6.1079.11fa7ebdntLNM9)
 
## Pod依赖

如果工程是通过 Pod 管理依赖，只需在 Podfile 中加入以下依赖，不需要再导入 Framework：

```
pod 'AliyunOSSiOS'

```

有关管理工具 Cocoapods 的更多详情，请参考文档: [CocoaPods安装和使用教程](http://code4app.com/article/cocoapods-install-usage)。

**说明** 您可以选择直接引入 Framework 或者 Pod 依赖两种方式中的任意一种。

## 工程中引入头文件

```
#import <AliyunOSSiOS/OSSService.h>

```
##兼容IPv6-Only网络

为了解决无线网络下域名解析容易遭到劫持的问题，OSS 移动端 SDK 引入了 HTTPDNS 进行域名解析，直接使用 IP 请求 OSS 服务端。在 IPv6-Only 的网络下可能会遇到兼容性问题。App 官方近期发布了关于 IPv6-only 网络环境兼容的 App 审核要求。为此，SDK 从 2.5.0 版本开始已经做了兼容性处理。在新版本中，除了 -ObjC 的设置，还需要引入两个系统库：

``` 
libresolv.tbd
CoreTelephony.framework
SystemConfiguration.framework
``` 
>如果已完成以上操作接下来就看看[GS_OSSMansger 点击可下载](https://gitee.com/Gsohnago/GS_OSSManager.git)吧！
- 代码示例：

``` 
/**
 上传头像图片到oss服务器上（是对OSS IOS SDK 中，上传和获取URL两个方法的合并封装）
 
 @param picName 自定义的图片文件名
 @param imgData 图片Data数据
 @param complete 返回图片地址
 @param failed  返回error信息
 */
- (void)headimg_uploadImgToOssImgPicName:(NSString *)picName
                                 imgData:(NSData *)imgData
                                complete:(void (^)(NSString * imgUrl))complete
                                  failed:(void (^)(NSError * error))failed;

/**
 上传图片到oss服务器上
 
 @param imgFileName 自定义的图片文件名，名字可以随意写，后缀名不能缺少，切记每次图片名都唯一
 @param imgData 图片Data数据
 */
- (void)uploadImgToOssImgFileName:(NSString *)imgFileName imgData:(NSData *)imgData;
/**
 获取oss上的图片url地址
 
 @param imgFileName imgFileName 图片文件名
 @return 返回图片的url地址
 */
- (NSString *)getImgAddress:(NSString *)imgFileName;
 ```
- 使用示例：

 ```
 [[GS_OSSManager sharedManager] headimg_uploadImgToOssImgPicName:@"GS_HeadIcon" imgData:_imgData complete:^(NSString * _Nonnull imgUrl) {
        NSLog(@"imgUrl:%@",imgUrl);
    } failed:^(NSError * _Nonnull error) {
        NSLog(@"error:%@",error);
    }];
 ```
- 注意⚠️（必填）初始化示例：

 ```
//在初始化 GS_OSSManager 这个单利对象的时候 
- (id)init {
    if (self = [super init]) {
        //默认参数:（必传）
        self.endpoint = @"http://oss-cn-region.aliyuncs.com";//!<访问域名此处填写的是例子
        self.accessKey = @"OSS生成的accessKey";
        self.secretKey = @"OSS生成的secretKey";
        self.bucketName = @"储存空间命名";
    }
    return self;
}

 ```
